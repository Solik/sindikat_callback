/* переменной isVisible присвоено значение НЕПРАВДА*/
var postfix = '_callbackForm';
var isVisible = false;
var form = document.getElementById('form' + postfix);
var greyScreen = document.getElementById('grey-screen' + postfix);

var button = document.getElementById('button' + postfix);
button.onclick = toggleFormVisibility;

var closeButton = document.getElementsByClassName('close' + postfix)[0];
closeButton.onclick = toggleFormVisibility;

var closeArea = document.getElementById('grey-screen' + postfix);
closeArea.onclick = toggleFormVisibility;

var screen1 = document.getElementById('screen-1'+ postfix);
/*создана ф-ция toggleFormVisibility - переключатель видимости формы*/
var screen2 = document.getElementById('screen-2'+ postfix);

var nameError = document.getElementById('correct-name' + postfix);
var phoneError = document.getElementById('correct-phone' + postfix);
var questionError = document.getElementById('correct-question' + postfix);
var nameInput = document.getElementById('name-input' + postfix);
var phoneInput = document.getElementById('phone-input' + postfix);



var messageInput = document.getElementById('question-input' + postfix);


function toggleFormVisibility(action) {
    //Переменная ignore нам нужна для предотвращения показа формы ввода там,
    //где это не нужно.
    var ignore; // изначально при создании переменной она является undefined
    // и, соответствено  - folse;

    if (action && action == 'closeOnly') ignore = true;

    if (!isVisible) {
        if (ignore) return; //итак, если ignore примет значение true - то не показывать форму.

        isVisible = true;
        form.style.display = 'block';
        greyScreen.style.display = 'block';

    } else {
        isVisible = false;
        form.style.display = 'none';
        greyScreen.style.display = 'none';
    }

}

var submitButton = document.getElementById('submit-button' + postfix);

submitButton.onclick = function(e) {
    //e.preventDefault() - позволяет прервать дефолтное поведение элемента в браузере
    //например, когда нажимаем на ссылку - запретить браузеру отсылать пользователя по ней,
    //и вместо этого выполнить необходимое программисту действие.
    e.preventDefault();



    var error = false;

// в переменную phoneNumber присвоили ранее назначенную переменную phone,
// и поскольку переменная была строкой, у нее есть встроенный метод
// replace(), который работает как "найти и заменить".
// В "найти" нужно вставлять регулярные выражения (regex, заключающиеся
// между слешами: /regex/). В данном случае /\D/ - означающее НЕЦИФРУ.
// g после регулярного выражения будет означать, что ищем не одно
// вхождение НЕЦИФРЫ, а все (от слова globaly). Т.к. вставляется
// пустая строка '', то мы меняем все нецифры - на ничто, т.е. УДАЛЯЕМ
// все нецифры.
    var phoneNumber = phoneInput.value.replace(/\D/g, '');
    console.log(phoneNumber);
    console.log(messageInput);

    cleanFormErrors();

    if (!nameInput.value.length || nameInput.value.length < 2) {
        nameError.style.display = 'inline-block';
        nameInput.style.borderColor = '#F11D0E';
        error = true;
    }

    if (!phoneInput.value.length || phoneNumber.length < 6) {
        phoneError.style.display = 'inline-block';
        phoneInput.style.borderColor = '#F11D0E';
        error = true;
    }

    if (!messageInput.value.length || messageInput.value.length < 20) {
        messageInput.style.borderColor = '#F11D0E';
        questionError.style.display = 'inline-block';

        error = true;
    }

    if (error) {
        return;
    }


    showSubmitMessage();
};
function cleanFormErrors () {
    //в данном случае - все выводимые ошибки будут очищаться для нового ввода данных и проверки.
    nameError.style.display = 'none';
    phoneError.style.display = 'none';
    questionError.style.display = 'none';
    nameInput.style.borderColor = 'lightgray';
    phoneInput.style.borderColor = 'lightgray';
    messageInput.style.borderColor = 'lightgray';
}
/**
 * Даннвя ф-ция призвана показывать несколько экранов, в т.ч. затемненный при вызове формы обратного звонка
 */
function showSubmitMessage (){

    screen1.style.display = 'none';
    screen2.style.display = 'block';
    form.style.height = '100px';
    form.style.top = '200px';
    document.getElementById('opposite-message' + postfix).innerHTML = nameInput.value + ', Ваш запрос отправлен, мы свяжемя с Вами в ближайшее время'
    setTimeout(oppositeMessageGone, 5000);

}
function oppositeMessageGone (){
    toggleFormVisibility('closeOnly');
}
